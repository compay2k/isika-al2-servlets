<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form action="FicheArticle" method="post">
	
	<h1>Fiche article</h1>

	<jsp:include page="boxPanier.jsp"></jsp:include>

	<h2>${article.libelle }</h2>
	<h3>prix : ${article.prix}</h3>

	<input type="hidden" name="idArticle" value="${article.id}"/>	
	<input type="text" name="quantite"/>

	<input type="submit" value="Ajouter au panier"/>

</form>

</body>
</html>