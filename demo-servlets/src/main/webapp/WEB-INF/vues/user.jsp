<%@ page import="fr.isika.al.beans.Utilisateur"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    
<jsp:useBean id="user" scope="session" class="fr.isika.al.beans.Utilisateur"></jsp:useBean>

	
<jsp:setProperty property="prenom" name="user" value="donald"/> 

<jsp:include page="entete.jsp"></jsp:include>
	
	<c:if test="${user.prenom != 'donald'}">
		<h2>Bienvenue � Paris</h2>
	</c:if>
	
	<c:choose>
		<c:when test="${user.prenom == 'bill'}">
			Salut billy
		</c:when>
		<c:when test="${user.prenom == 'bob'}">
			Salut robert
		</c:when>
		<c:when test="${user.prenom == 'jack'}">
			Salut jackie
		</c:when>
		<c:otherwise>
			Salut toi...
		</c:otherwise>
	</c:choose>
	
	<br/>
	
	<c:forEach begin="0" end="10" step="1" var="i">
		${i}
		<br/>
	</c:forEach>
	
	<c:forEach items="${maListe}" var="monItem">
		${monItem.nom}
	</c:forEach>
	
	<h1>details utilisateur</h1>
	${user.nom}
	<jsp:getProperty property="nom" name="user"/>

<jsp:include page="pied.jsp"></jsp:include>	