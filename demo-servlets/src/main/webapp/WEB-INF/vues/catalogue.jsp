<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="articles" scope="application" class="java.util.ArrayList"></jsp:useBean>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<h1>Catalogue</h1>

	<jsp:include page="boxPanier.jsp"></jsp:include>

	<ul>
	<c:forEach items="${articles}" var="article">
		<li>
		<a href="FicheArticle?id=${article.id}">${article.libelle}</a> 	
		</li>
	</c:forEach>
	</ul>

</body>
</html>