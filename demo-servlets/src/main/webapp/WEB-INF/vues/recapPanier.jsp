<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<h1>Recap panier</h1>

<jsp:include page="boxPanier.jsp"></jsp:include>

<a href="Catalogue">retour</a>

<table>
	<tr>
		<td>libelle</td>
		<td>PU</td>
		<td>Qte</td>
		<td>Total</td>
	</tr>
<c:forEach items="${panier.lignes}" var="ligne">
	<tr>
		<td>${ligne.article.libelle}</td>
		<td>${ligne.article.prix}</td>
		<td>${ligne.quantite}</td>
		<td>${ligne.article.prix * ligne.quantite}</td>
	</tr>
</c:forEach>
</table>
	

</body>
</html>