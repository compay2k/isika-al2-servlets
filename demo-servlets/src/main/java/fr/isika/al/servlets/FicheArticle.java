package fr.isika.al.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.isika.al.beans.Article;
import fr.isika.al.beans.LignePanier;
import fr.isika.al.beans.Panier;

public class FicheArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public FicheArticle() {
        super();
    }

    private Article getArticle(int idArticle)
    {
    	List<Article> articles = (List<Article>)getServletContext().getAttribute("articles");
		
		Article article = articles.stream()
				.filter(a -> a.getId() == idArticle)
				.findFirst()
				.get();
		
		return article;
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�rer l'article demand� :
		int idArticle = Integer.parseInt(request.getParameter("id"));
		
		Article article = getArticle(idArticle);
		
		request.setAttribute("article", article);
		
		// forwarder � une vue
		getServletContext().getRequestDispatcher("/WEB-INF/vues/ficheArticle.jsp")
							.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//r�cup�rer l'idArticle et la quantite : 
		int idArticle = Integer.parseInt(request.getParameter("idArticle"));
		int quantite = Integer.parseInt(request.getParameter("quantite"));
		
		// r�cup�rer l'article demand� :
		Article article = getArticle(idArticle);
		
		// cr�er une ligne de panier :
		LignePanier ligne = new LignePanier();
		ligne.setArticle(article);
		ligne.setQuantite(quantite);
		
		Panier p = (Panier)request.getSession().getAttribute("panier");
		
		p.getLignes().add(ligne);
		
		response.sendRedirect("RecapPanier");
	}

}
