package fr.isika.al.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.isika.al.beans.Utilisateur;

public class UtilisateurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UtilisateurServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		Utilisateur u = new Utilisateur();
		u.setNom(request.getParameter("nom"));
		u.setPrenom(request.getParameter("prenom"));
		
		// �criture dans la session :
		request.getSession().setAttribute("user", u);
		
		
		String couleur = request.getParameter("couleur");
		
		// Ecrire dans un cookie :
		Cookie c = new Cookie("couleur", couleur);
		c.setMaxAge(3600*24*7);
		response.addCookie(c);
				
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.getWriter().append("bonjour " + u.getPrenom() + " " + u.getNom());
	}

}
