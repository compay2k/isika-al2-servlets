package fr.isika.al.servlets;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyFirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private String langue;
	private int ageDuCapitaine;
	
    public MyFirstServlet() 
    {
    	super();
    	System.out.println("Constructeur de ma servlet");
    }

    @Override
	public void init(ServletConfig config) throws ServletException 
    {
    	super.init(config);
    	this.langue = config.getInitParameter("langue");
    	this.ageDuCapitaine = Integer.parseInt(config.getInitParameter("ageDuCapitaine"));
    	System.out.println("Initialisation de ma servlet");
	}

    @Override
	public void destroy() 
    {
    	System.out.println("Desctruction de ma servlet");
	}

    @Override
	public String getServletInfo() {
    	
    	String infos = "";
    	switch(this.langue)
    	{
	    	case "fr" :
	    		infos = "Je suis une servlet d'accueil";
	    		break;
	    	default :
	    		infos = "I am a welcome servlet";
	    		break;
    	}
    	
		return infos; 
	}

    @Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
    	System.out.println("Service de ma servlet");
    	super.service(request, response);
	}

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
    	System.out.println("Traitement du get de ma servlet");
		response.getWriter()
					.append("Served at: ")
					.append(request.getContextPath())
					.append("<br/>")
					.append(getServletInfo());
		
		//response.setContentType("text/html;charset=UTF-8");
		response.addHeader("Content-Type", "text/html;charset=UTF-8");
		response.setStatus(418);
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
    	// r�cup�rer un param�tre de requete :
    	String s = request.getParameter("toto");
		doGet(request, response);
	}

}
