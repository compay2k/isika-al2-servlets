package fr.isika.al.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.isika.al.beans.Utilisateur;

public class FichePersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   public FichePersonne() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		// r�cup�ration de l'utilisateur en session :
//		Utilisateur u = (Utilisateur)request.getSession()
//											.getAttribute("user");
//		
//		// pas d'utlisateur en session : on l'ajoute
//		if (u == null)
//		{
//			u = new Utilisateur();
//			u.setNom("Stallman");
//			request.getSession().setAttribute("user", u);
//		}
		
		//request.setAttribute("user", u);
		
		ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
		
		Utilisateur u1 = new Utilisateur();
		u1.setNom("Gates");
		
		Utilisateur u2 = new Utilisateur();
		u2.setNom("Lovelace");
		
		liste.add(u1);
		liste.add(u2);
		
		Utilisateur usr = liste.stream()
								.filter(u -> u.getNom() == "Gates")
								.findFirst()
								.get();
		liste.add(usr);
		request.setAttribute("maListe", liste);
		
		// transfert � une vue :
		getServletContext().getRequestDispatcher("/WEB-INF/vues/user.jsp")
						   .forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
