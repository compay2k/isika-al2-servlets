package fr.isika.al.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.isika.al.beans.Utilisateur;

/**
 * Servlet implementation class AfficherCookies
 */
public class AfficherCookies extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherCookies() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		// lecture de cookies :
		for(int i = 0; i < request.getCookies().length; i++)
		{
			Cookie c = request.getCookies()[i];
			response.getWriter().append(" - " + c.getName() + " : " + c.getValue() + "\n");
		}
			
		// lecture dans la session :
		Utilisateur u = (Utilisateur)request.getSession().getAttribute("user");
		response.getWriter().append(u.getPrenom() + "\n");
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
