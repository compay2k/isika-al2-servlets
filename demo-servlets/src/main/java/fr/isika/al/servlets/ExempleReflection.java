package fr.isika.al.servlets;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.isika.al.beans.Article;
import fr.isika.al.beans.ITruc;

/**
 * Servlet implementation class ExempleReflection
 */
public class ExempleReflection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExempleReflection() {
        super();
        // TODO Auto-generated constructor stub
    }


    private void exempleXml()
    {
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	
    	 try {
    		 // récupération du codument xml :     		 
    		 DocumentBuilder builder = factory.newDocumentBuilder();
             File fileXML = new File("D:\\test\\plop.xml");
             Document xml = builder.parse(fileXML);
             
             NodeList nodes = xml.getElementsByTagName("toto");
             
             for (int i = 0; i < nodes.getLength(); i++) 
             { 
            	 Element elt = (Element)nodes.item(i);
            	 
            	 Attr attr = elt.getAttributeNode("id");
            	 
            	 String valeurAttribut = attr.getValue();
            	 String valeurElement = elt.getTextContent();
            	 
            	 System.out.println(" - " + valeurAttribut + " : " + valeurElement);
             }
             
    	 }
    	 catch(Exception e)
    	 {
    		 e.printStackTrace();
    	 }
    	
    	
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println(request.getServletPath());
		
		//Article a = new Article();
		//a.plop("plop", 10);
		
		String cname = "fr.isika.al.beans.Article";
		
		try {
			
			Class c = Class.forName(cname);
			Object o = c.newInstance();
			
			//ITruc t = (ITruc)o;
			//t.plop("aaa", 10);
			
			Class[] paramTypes = {String.class, int.class};
			Method m = c.getMethod("plop", paramTypes);
			
			Object[] params = {"aaa",10};
			m.invoke(o, params);
			
			exempleXml();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
