package fr.isika.al.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.isika.al.beans.Article;

public class InitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   public InitServlet() {
        super();
    }

   @Override
   public void init(ServletConfig config) throws ServletException 
   {
	   super.init(config);
	   List<Article> articles = new ArrayList<Article>();
	   
	   articles.add(new Article(1, "Pelle", 12.5));
	   articles.add(new Article(2, "Brouette", 47));
	   articles.add(new Article(3, "Rateau", 18.5));
	   articles.add(new Article(4, "Arrosoir", 9.99));
	   articles.add(new Article(5, "Seau", 15));
	   
	   getServletContext().setAttribute("articles", articles);
	   System.out.println("Liste d'articles charg�e");
   }

}
