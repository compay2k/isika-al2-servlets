package fr.isika.al.taglibs;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class MyFirstTag extends TagSupport
{
	private static final long serialVersionUID = 1L;
	
	private String adresse;
	
	public void setAdresse(String adresse)
	{
		this.adresse = adresse;
	}
	
	@Override
	public int doStartTag() throws JspException {

		JspWriter out = pageContext.getOut();
		try 
		{
			if (adresse == "")
			{
				adresse = "#";
			}
			
			boolean lienExterne = adresse.startsWith("http://");
			
			out.write("<a href=\"" + adresse + "\" ");
			if (lienExterne)
			{
				out.write("target=\"_new\"");
			}
			out.write(">");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return EVAL_BODY_INCLUDE;
	}
	
	
	@Override
	public int doEndTag() throws JspException 
	{
		JspWriter out = pageContext.getOut();
		
		try 
		{
			out.write("</a>");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return EVAL_PAGE;
	}
	
}
