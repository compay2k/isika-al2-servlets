package fr.isika.al.taglibs;

import java.io.IOException;
import java.time.temporal.ValueRange;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class CustomFormatTag extends TagSupport
{
	private static final long serialVersionUID = 1L;
	
	private String valeur;
	
	public void setValeur(String valeur)
	{
		this.valeur = valeur;
	}
	
	@Override
	public int doStartTag() throws JspException 
	{	
		JspWriter out = pageContext.getOut();
		
		try {
			String[] mots = valeur.split(" ");
			
			for (String mot : mots) 
			{
				out.append(mot.substring(0, 1).toUpperCase());
				out.append(mot.substring(1).toLowerCase());	
				out.append(" ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}
}
