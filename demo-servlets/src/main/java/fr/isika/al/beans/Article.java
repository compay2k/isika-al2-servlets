ackage fr.isika.al.beans;

import java.io.Serializable;

public class Article implements Serializable, ITruc
{
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String libelle;
	private double prix;
	
	public Article()
	{
		
	}

	public Article(int id, String libelle, double prix) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.prix = prix;
	}


	public void plop(String s, int x)
	{
		for (int i = 0; i < x; i++) 
		{
			System.out.println(s);
		}
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	
	
	
	
}
