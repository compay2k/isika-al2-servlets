package fr.isika.al.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Panier implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private Date dateCreation;
	
	private List<LignePanier> lignes = new ArrayList<LignePanier>();

	public int getNbArticles()
	{
		int resultat = 0;
		
		for (LignePanier ligne : lignes) 
		{
			resultat += ligne.getQuantite();
		}
		
		return resultat;
	}
	
	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<LignePanier> getLignes() {
		return lignes;
	}

	public void setLignes(List<LignePanier> lignes) {
		this.lignes = lignes;
	}
	
	
	
}
