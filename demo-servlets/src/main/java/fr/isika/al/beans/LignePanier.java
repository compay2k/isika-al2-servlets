package fr.isika.al.beans;

import java.io.Serializable;

public class LignePanier implements Serializable
{
	private static final long serialVersionUID = 1L;

	private int quantite;
	private Article article;
	
	public int getQuantite() 
	{
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	
	
	
}
